//
//  FLCGUIHandler.m
//  FlawlessClient
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "FLCGUIHandler.h"
#import "FLCSocket.h"

@implementation FLCGUIHandler

- (IBAction)connectToServer:(id)sender {
    if([[FLCSocket shared] isConnected])
        [self log:@"Already connected"];
    else {
        [self log:@"Connecting to server"];
        [[FLCSocket shared] initClient: [messageToSend stringValue]];
        [self log:[NSString stringWithFormat:@"with sending message: %@", [messageToSend stringValue]]];
        [[FLCSocket shared] start];
    }
}

- (IBAction)disconnectFromServer:(id)sender {
    [self log:@"Disconnecting from server"];
    [[FLCSocket shared] disconnectClient];
    [[FLCSocket shared] cancel];
//  [[FLCSocket shared].obj_client = NULL;
}

- (IBAction)send:(id)sender {
    [self log:[NSString stringWithFormat:@"Sending: %@", [messageToSend stringValue]]];
    [[FLCSocket shared] sendData:[messageToSend stringValue]];
    [messageToSend setStringValue:@""];
}

- (void) log:(NSString *) msg {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *text = msg;
        NSString *log = [logText stringValue];
        
        if ((log != nil) && ([log length] > 0)) {
            text = [NSString stringWithFormat:@"%@\n%@", [logText stringValue], msg];
        }
        
        if ([[logText stringValue] length] > 5000) {
            text = [text substringFromIndex:3000];
        }
        [logText setStringValue:text];
    });
}

@end
