//
//  FLCSocket.m
//  FlawlessClient
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "FLCSocket.h"
#import "FLCGUIHandler.h"

@implementation FLCSocket

+ (FLCSocket *) shared
{
    static dispatch_once_t predicate = 0;
    __strong static FLCSocket *sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (void) initClient: (NSString *) withSendMessage {
    const char *infoMessage = [withSendMessage UTF8String];
    CFSocketContext socketContext = {0, infoMessage, NULL, NULL, NULL};
    _obj_client = CFSocketCreate(kCFAllocatorDefault, AF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketConnectCallBack, TCPClientCallBackHandler, &socketContext);
    struct sockaddr_in sock_addr;
    memset(&sock_addr, 0, sizeof(sock_addr));
    sock_addr.sin_len = sizeof(sock_addr);
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons(6658);
    inet_pton(AF_INET, "127.0.0.1", &sock_addr.sin_addr);
    CFDataRef dref = CFDataCreate(kCFAllocatorDefault, (UInt8*)&sock_addr, sizeof(sock_addr));
    CFSocketConnectToAddress(_obj_client, dref, -1);
    CFRelease(dref);
    self.isConnectedToServer = YES;
    
}

void TCPClientCallBackHandler(CFSocketRef s, CFSocketCallBackType callbackType, CFDataRef address, const void *data, void *info) {
    switch (callbackType) {
        case kCFSocketConnectCallBack:
            ConnectCallBack(s, kCFSocketAcceptCallBack, address, data, info);
            break;
        default:
            break;
    }
}

void ConnectCallBack(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, const void *data,
                     void *info)
{
    UInt8 buffer[128];
    
    CFSocketNativeHandle sock = CFSocketGetNative(socket);
    
    char *joke = info;
    
    printf("OK. %s", joke);
    
    send(sock, joke, strlen(joke)+1, 0);
    recv(sock, buffer, sizeof(buffer), 0);    
}


- (void) sendData: (NSString *) messageToSend
{
    const char *sendString = [messageToSend UTF8String];
    CFDataRef data = CFDataCreate(NULL, (const UInt8*)sendString, strlen(sendString));
    CFSocketError errorSocket = CFSocketSendData(_obj_client, NULL, data, 0);
    if (errorSocket != kCFSocketSuccess) {
        NSLog(@"Error in sending data to the server");
        CFRelease(data);
    }
}

- (void) initNative:(CFSocketNativeHandle) native_scoket{
    CFSocketContext socketContext = {0, (__bridge void*)(self), NULL, NULL, NULL};
    _obj_client = CFSocketCreateWithNative(kCFAllocatorDefault, native_scoket, kCFSocketReadCallBack, TCPClientCallBackHandler, &socketContext);
}

- (void) main {
    CFRunLoopSourceRef loopRef = CFSocketCreateRunLoopSource(kCFAllocatorDefault, _obj_client, 0);
    CFRunLoopAddSource(CFRunLoopGetCurrent(), loopRef, kCFRunLoopDefaultMode);
    CFRelease(loopRef);
    CFRunLoopRun();
}

- (void) disconnectClient {
    CFSocketInvalidate(_obj_client);
    CFRelease(_obj_client);
    CFRunLoopStop(CFRunLoopGetCurrent());
    _obj_client = NULL;
    self.isConnectedToServer = NO;
    
}


- (BOOL) isConnected {
    return self.isConnectedToServer;
}



@end
