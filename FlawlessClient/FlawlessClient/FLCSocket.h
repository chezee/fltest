//
//  FLCSocket.h
//  FlawlessClient
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>

@interface FLCSocket : NSThread{
}
@property (nonatomic, readwrite) CFSocketRef obj_client;
@property (nonatomic) BOOL isConnectedToServer;

+ (FLCSocket *) shared;
- (void) initClient:(NSString *) withSendMessage;
- (void) main;
- (void) initNative:(CFSocketNativeHandle) native_scoket;
- (void) sendData: (NSString *)messageToSend;

- (void) disconnectClient;
- (BOOL) isConnected;

@end
