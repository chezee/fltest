//
//  main.m
//  FlawlessClient
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
