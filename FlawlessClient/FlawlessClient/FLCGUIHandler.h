//
//  FLCGUIHandler.h
//  FlawlessClient
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

@interface FLCGUIHandler : NSObject {
    __weak IBOutlet NSTextField *logText;
    __weak IBOutlet NSTextField *messageToSend;
}
- (IBAction)connectToServer:(id)sender;
- (IBAction)disconnectFromServer:(id)sender;
- (IBAction)send:(id)sender;
- (void) log:(NSString *) msg;

@end
