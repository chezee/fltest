//
//  FLSGUIHandler.m
//  FlawlessServers
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "FLSGUIHandler.h"

@implementation FLSGUIHandler

- (IBAction)startServer:(id)sender {
    if(![[FLSSocket shared] isUp]){
        [[FLSSocket shared] initServer];
        [self log:@"Server initialized" toTextView:logText];
        [[FLSSocket shared] start];
        [self log:@"Server started" toTextView:logText];
    }
    else
        [self log:@"Server is up already" toTextView:logText];
}

- (IBAction)stopServer:(id)sender {
    [self log:@"Stoping server" toTextView:logText];
    [[FLSSocket shared] stopServer];
    [[FLSSocket shared] cancel];
    [self log:@"Server stoped" toTextView:logText];
}

- (void) recievedMessage {
    
    
}

- (void) log:(NSString *) msg toTextView:(NSTextField *)textView{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *text = msg;
        NSString *log = [textView stringValue];
        
        if ((log != nil) && ([log length] > 0)) {
            text = [NSString stringWithFormat:@"%@\n%@", [textView stringValue], msg];
        }
        
        if ([[textView stringValue] length] > 5000) {
            text = [text substringFromIndex:3000];
        }
        [textView setStringValue:text];
    });
    
}

@end
