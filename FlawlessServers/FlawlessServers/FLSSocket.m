//
//  FLSSocket.m
//  FlawlessServers
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "FLSSocket.h"

@implementation FLSSocket {
    CFReadStreamRef readStream;
}

+ (FLSSocket *) shared
{
    static dispatch_once_t predicate = 0;
    __strong static FLSSocket *sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}


- (void) initServer{
    if(obj_server){
        NSLog(@"Server is up");
            }
    else {
        char infoMessage[] = "Hello from server";
        CFSocketContext sc = {0, infoMessage, NULL, NULL, NULL};
        obj_server = CFSocketCreate(kCFAllocatorDefault, AF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack,    TCPServerCallbackHandler, &sc);
        struct sockaddr_in sock_addr;
        memset(&sock_addr, 0, sizeof(sock_addr));
        sock_addr.sin_len = sizeof(sock_addr);
        sock_addr.sin_family = AF_INET;
        sock_addr.sin_port = htons(2048);
        sock_addr.sin_addr.s_addr = INADDR_ANY;
    
        CFDataRef dref = CFDataCreate(kCFAllocatorDefault, (UInt8*)&sock_addr, sizeof(sock_addr));
        CFSocketSetAddress(obj_server, dref);
        CFRelease(dref);
        self.isUp = YES;
    }
}

- (void) main {
    CFRunLoopSourceRef loopRef = CFSocketCreateRunLoopSource(kCFAllocatorDefault, obj_server, 0);
    CFRunLoopAddSource(CFRunLoopGetCurrent(), loopRef, kCFRunLoopCommonModes);
    CFRelease(loopRef);
    CFRunLoopRun();
}

void TCPServerCallbackHandler(CFSocketRef s, CFSocketCallBackType callbackType, CFDataRef address, const void *data, void *info) {
    switch (callbackType) {
        case kCFSocketAcceptCallBack:
                acceptCallBack(s, callbackType, address, data, info);
            break;
        case kCFSocketDataCallBack:
                readCallBack(s, callbackType, address, data, info);
            break;
        default:
            break;
    }
    
}

    
void acceptCallBack(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, const void *data,
                         void *info)
    {
        readStream = NULL;
        CFWriteStreamRef writeStream = NULL;
        CFIndex bytes = 0;
        UInt8 buffer[128];
        UInt8 recv_len = 0, send_len = 0;
        
        CFSocketNativeHandle sock = *(CFSocketNativeHandle *) data;
        
        char *punchline = info;
        
        CFStreamCreatePairWithSocket(kCFAllocatorDefault, sock,
                                     &readStream, &writeStream);
        
        if (!readStream || !writeStream) {
            close(sock);
            fprintf(stderr, "CFStreamCreatePairWithSocket() failed\n");
        }
        
        CFReadStreamOpen(readStream);
        CFWriteStreamOpen(writeStream);
        
        memset(buffer, 0, sizeof(buffer));
        while (!strchr((char *) buffer, '\n') && recv_len < sizeof(buffer)) {
            bytes = CFReadStreamRead(readStream, buffer + recv_len,
                                     sizeof(buffer) - recv_len);
            if (bytes < 0) {
                fprintf(stderr, "CFReadStreamRead() failed: %ld\n", bytes);
            }
            recv_len += bytes;
        }
        
        while (send_len < (strlen(punchline+1))) {
            if (CFWriteStreamCanAcceptBytes(writeStream)) {
                bytes = CFWriteStreamWrite(writeStream,
                                           (unsigned char *) punchline + send_len,
                                           (strlen((punchline)+1) - send_len) );
                if (bytes < 0) {
                    fprintf(stderr, "CFWriteStreamWrite() failed\n");
                }
                send_len += bytes;
            }
        }
        
}

void readCallBack(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, const void *data, void *info)
{
    NSData *nsData = (NSData *)CFBridgingRelease(data);
    if ([nsData length] == 0) {
        NSLog(@"Connection dropped by remote socket");
    }
    CFDataRef df = (CFDataRef) data;
    int len = CFDataGetLength(df);
    CFRange range = CFRangeMake(0,len);
    UInt8 buffer[len+1];
    
    if(len <= 0) {
        NSLog(@"No data read");
    }
    memset((void*)buffer,0,sizeof(buffer));
    CFDataGetBytes(df, range, buffer);
    NSString *byteString = [[NSString alloc] initWithBytes:buffer length:len encoding:NSUTF8StringEncoding];
    NSLog(@"byte string is : %@",byteString);
}

- (void) stopServer {
    CFSocketInvalidate(obj_server);
    CFRelease(obj_server);
    CFRunLoopStop(CFRunLoopGetCurrent());
    self.isUp = NO;
}

- (BOOL) connected {
    return self.isUp;
}

@end

