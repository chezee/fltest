//
//  FLSSocket.h
//  FlawlessServers
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>

@interface FLSSocket : NSThread{
    CFSocketRef obj_server;
}

@property (nonatomic) BOOL isUp;
@property (atomic, weak) NSString *recvString;

+ (FLSSocket *) shared;
- (void) main;
- (void) initServer;
- (void) stopServer;

@end
