//
//  FLSGUIHandler.h
//  FlawlessServers
//
//  Created by Илья Пупкин on 11/21/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "FLSSocket.h"

@interface FLSGUIHandler : NSObject {
    __weak IBOutlet NSTextField *logText;
    FLSSocket *obj_server_socket;
}

- (IBAction)startServer:(id)sender;
- (IBAction)stopServer:(id)sender;


@end
